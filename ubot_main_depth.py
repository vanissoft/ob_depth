import threading
import time
from gi.repository import GLib
import trio
import json
import msgpack
import pandas as pd

import ubot.comm as comm
import ubot_gui_depth as gui
from queue import Queue

WORKER = {}

class Main():
    def __init__(self):
        self.gui = gui
        self.ws = None
        self.currentPrice = 0
        self.tasks = Queue()
        self.subscriber_depth_binancef = comm.Subscriber(['depth_btcusdt', 'depth_eosusdt'], port=15001)
        self.subscriber_depth_bitstamp = comm.Subscriber(['depth_btcusd'], port=15002)
        self.subscriber_depth_coinbasepro = comm.Subscriber(['depth_btc-usd'], port=15004)
        self.subscriber_depth_bitfinex = comm.Subscriber(['depth_btcusd'], port=15005)

    async def binancef_subscriber(self, topic, data):
        data = msgpack.loads(data, raw=False)
        df = pd.DataFrame(data)
        print('1', end='')
        gui.TASKS.put_nowait({'cmd': 'depth1', 'data': [topic, df]})
        GLib.idle_add(gui.GUI.update)

    async def bitstamp_subscriber(self, topic, data):
        data = msgpack.loads(data, raw=False)
        df = pd.DataFrame(data)
        print('2', end='')
        gui.TASKS.put_nowait({'cmd': 'depth2', 'data': [topic, df]})
        GLib.idle_add(gui.GUI.update)

    async def coinbasepro_subscriber(self, topic, data):
        data = msgpack.loads(data, raw=False)
        df = pd.DataFrame(data)
        print('3', end='')
        gui.TASKS.put_nowait({'cmd': 'depth3', 'data': [topic, df]})
        GLib.idle_add(gui.GUI.update)

    async def bitfinex_subscriber(self, topic, data):
        data = msgpack.loads(data, raw=False)
        df = pd.DataFrame(data)
        print('4', end='')
        gui.TASKS.put_nowait({'cmd': 'depth4', 'data': [topic, df]})
        GLib.idle_add(gui.GUI.update)

    async def timer1(self):
        i = 0
        while True:
            await trio.sleep(20)
            GLib.idle_add(gui.GUI.update)

    async def timer2(self):
        while True:
            await trio.sleep(0.01)
            try:
                t = self.tasks.get_nowait()
            except:
                continue
            if t['cmd'] == 'close_order':
                await self.position.cancel_order(t['id'])
            elif t['cmd'] == 'close_order_all':
                await self.position.cancel_all()
            elif t['cmd'] == 'close_order_shorts':
                await self.position.cancel_sl('SELL')
            elif t['cmd'] == 'close_order_longs':
                await self.position.cancel_sl('BUY')
            elif t['cmd'] == 'close_position':
                await self.position.close_position()
            elif t['cmd'] == 'change_margin':
                await self.position.change_margin(t['amount'])
            elif t['cmd'] == 'price_gauge':
                gui.TASKS.put_nowait({'cmd': 'info_price_gauge', 'data': self.statistics.price_gauge(t['data'])})
                gui.TASKS.put_nowait({'cmd': 'set_price_gauge', 'data': self.statistics.price_gauge(t['data'])})
            elif t['cmd'] == 'price_wide':
                gui.TASKS.put_nowait({'cmd': 'set_price_wide', 'data': (self.currentPrice, t['data'])})
            elif t['cmd'] == 'price_adjust':
                gui.TASKS.put_nowait({'cmd': 'set_price_adjust', 'data': t['data']})
            elif t['cmd'] == 'amount_increase':
                print(f"main > {t['cmd']} {t['data']}")
                gui.TASKS.put_nowait({'cmd': 'set_amount', 'data': t['data']})
            elif t['cmd'] == 'amount_reset':
                gui.TASKS.put_nowait({'cmd': 'set_amount_0', 'data': 0})
            elif t['cmd'] == 'post_order':
                await self.position.order_put(*t['data'])
            elif t['cmd'] == 'post_order_market':
                await self.position.order_put_market(*t['data'])

    async def timer3(self):
        while True:
            await trio.sleep(30)
            try:
                await self.position.get_info()
            except:
                pass
                #self.cancel_scope.cancel()
            #gui.TASKS.put_nowait({'cmd': 'info_wallet', 'data': self.position.wallet})
            GLib.idle_add(gui.GUI.update)

    async def workers(self):
        from subprocess import Popen, PIPE
        for w in ["bitstamp", "coinbasepro", "binancef", "bitfinex"]:
            WORKER[w] = Popen(["python", f"ubot_{w}_listener.py"],
                               stdin=PIPE,
                               stdout=PIPE,
                               stderr=PIPE,
                               universal_newlines=True,
                               bufsize=0)
        worker_std = {}
        for w in WORKER:
            worker_std[w] = (trio.wrap_file(WORKER[w].stdout),trio.wrap_file(WORKER[w].stderr))
        while True:
            await trio.sleep(.1)
            for w in WORKER:
                std = await worker_std[w][0].readline()
                print(w, std, end='')


    async def start(self):

        def thread():
            gui.init(self.tasks)

        thread = threading.Thread(target=thread)
        thread.daemon = False
        thread.start()
        time.sleep(.1)

        if gui.GUI is None:
            quit(0)

        self.subscriber_depth_binancef.on_receive = self.binancef_subscriber
        self.subscriber_depth_bitstamp.on_receive = self.bitstamp_subscriber
        self.subscriber_depth_coinbasepro.on_receive = self.coinbasepro_subscriber
        self.subscriber_depth_bitfinex.on_receive = self.bitfinex_subscriber

        async with trio.open_nursery() as nur:
            self.cancel_scope = nur.cancel_scope
            nur.start_soon(self.subscriber_depth_binancef.start)
            nur.start_soon(self.subscriber_depth_bitstamp.start)
            nur.start_soon(self.subscriber_depth_coinbasepro.start)
            nur.start_soon(self.subscriber_depth_bitfinex.start)
            nur.start_soon(self.workers)
            nur.start_soon(self.timer1)
            nur.start_soon(self.timer2)
            nur.start_soon(self.timer3)


if __name__ == "__main__":
    trio.run(Main().start)
    for w in WORKER:
        WORKER[w].stdin.close()
    print("exit")