"""
CoronaTrader!!!!  Cryptolandia Edition
(c) Elias
"""


import time
import gi
from queue import Queue
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, GObject, Pango
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)
from matplotlib.figure import Figure
import matplotlib
import numpy as np
import utils
from datetime import datetime

GUI = None
GUI_EXIT = False
TASKS = Queue()
TASKS_MAIN = None


class Window(Gtk.Window):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.listbox1 = None
        self.listbox1_data = set()
        self.orders_balance = 0
        self.grid1 = None
        self.figure1 = {}


    def compose(self):
        self.set_border_width(5)
        vpaned0 = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL)
        vpaned0.wide_handle = True
        vpaned0.props.margin = 5
        self.add(vpaned0)

        vbox0 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        vpaned0.add1(vbox0)

        fr1 = Gtk.Frame(label="  Position")
        fr1.set_border_width(5)
        fr1.props.margin = 5
        vbox0.pack_start(fr1, True, True, 0)

        matplotlib.rc('axes', edgecolor='#505050')
        matplotlib.rc('xtick', color='#909090')
        matplotlib.rc('ytick', color = '#909090')
        matplotlib.rc('figure', facecolor = 'white')
        # Temporary rc param
        f = Figure(figsize=(6, 4), dpi=100)
        self.figure1['figure'] = f
        f.blit = False
        #f.patch.set_facecolor('#A0A0A0')
        f.patch.set_alpha(0)
        ax = f.add_subplot(111)
        #ax.patch.set_facecolor('#A0A0A0')
        ax.patch.set_alpha(0)
        self.figure1['ax'] = ax
        x = np.linspace(0, 6*np.pi, 100)
        #self.figure1['data1'], self.figure1['data2'], self.figure1['data3']  = self.figure1['ax'].plot(x, np.sin(x+.2), np.sin(x+.1), np.sin(x+.3))
        self.figure1['data1'] = self.figure1['ax'].plot(x, np.sin(x + .1), linestyle='solid', color='#E0A060')[0]  # binance
        self.figure1['data2'] = self.figure1['ax'].plot(x, np.sin(x + .2), linestyle='solid', color='#00E090')[0]  # bitstamp
        self.figure1['data3'] = self.figure1['ax'].plot(x, np.sin(x + .3), linestyle='solid', color='#A02050')[0]  # coinbase
        self.figure1['data4'] = self.figure1['ax'].plot(x, np.sin(x + .4), linestyle='solid', color='#20A020')[0]  # bitfinex
        ax.legend(["binance", "bitstamp", "coinbase", "bitfinex"])
        ax.yaxis.label.set_color('#CCCCCC')
        ax.xaxis.label.set_color('#AAAAAA')
        canvas = FigureCanvas(f)  # a Gtk.DrawingArea
        canvas.set_size_request(600, 400)
        fr1.add(canvas)





class Gui:
    def __init__(self, tasks):
        self.window = Window(title='Binance Coronatrader!!! XD')
        self.window.app = self
        self.tasks = tasks
        self.window.compose()
        self.window.connect("destroy", self.exit)
        self.limits = [5000, 5400]

    def start(self):
        self.window.show_all()
        Gtk.main()

    def on_switch_activated(self, obj, param):
        print(obj.get_active())

    def tick(self, data):
        #self.window.label1.set_markup(f'<span foreground="#Faf04F">{data}</span> is <i>cool</i>!')
        pass

    def info_wallet(self, data):
        if not data:
            return
        self.window.labels['balance'][3].set_markup(f'<span foreground="cyan"><b>{data[1]:4,.2f}</b> {data[0]}</span>')

    def info_current(self, data):
        self.window.labels['current'][3].set_markup(f'<span foreground="white"><b>{data:4,.2f}</b></span>')

    def info_price_gauge(self, data):
        print(data)
        self.window.price_gauge.set_markup(f'<span foreground="yellow">{data[0]:8,.2f}</span> - <span foreground="yellow">{data[1]:8,.2f}</span>')

    def set_price_gauge(self, data):
        self.window.order_price1.set_text(str(round(data[0], 0)))
        self.window.order_price2.set_text(str(round(data[1], 0)))
    def set_price_wide(self, data):
        self.window.order_price1.set_text(str(round(data[0]-data[1], 0)))
        self.window.order_price2.set_text(str(round(data[0]+data[1], 0)))
    def set_price_adjust(self, data):
        self.window.order_price1.set_text(str(round(float(self.window.order_price1.get_text()) - data, 0)))
        self.window.order_price2.set_text(str(round(float(self.window.order_price2.get_text()) + data, 0)))
    def set_amount_0(self, _):
        self.window.order_amount.set_text(str(0))
    def set_amount(self, data):
        try:
            amnt = float(self.window.order_amount.get_text())
        except:
            amnt = 0
        amnt = round(amnt + data, 3)
        while amnt % 0.002 != 0:
            amnt = round(amnt+0.001, 3)
        self.window.order_amount.set_text(str(amnt))

    def info_positions(self, data):
        pos = data[0]
        if len(pos) == 0:
            self.window.labels['entry'][3].set_markup('')
            self.window.labels['liquidation'][3].set_markup('')
            self.window.labels['amount'][3].set_markup('')
            self.window.labels['profit'][3].set_markup('-')
            self.window.btn_close_position_frame.modify_bg(Gtk.StateType.NORMAL, Gdk.color_parse("black"))
        else:
            pos = pos[0]
            if data[1] > 0:
                pnl = (pos["positionAmt"] * data[1]) - (pos["positionAmt"] * pos["entryPrice"])
            else:
                pnl = 0
            if pos["positionAmt"] > 0:
                self.window.btn_close_position_frame.modify_bg(Gtk.StateType.NORMAL, Gdk.color_parse("green"))
            else:
                self.window.btn_close_position_frame.modify_bg(Gtk.StateType.NORMAL, Gdk.color_parse("red"))
            self.window.labels['entry'][3].set_markup(f'<span foreground="yellow">{pos["entryPrice"]:6,.0f}</span>')
            self.window.labels['liquidation'][3].set_markup(f'<span foreground="yellow">{pos["liquidationPrice"]:6,.0f}</span>')
            self.window.labels['amount'][3].set_markup(f'<span foreground="yellow">{pos["positionAmt"]:7,.3f}</span>')
            self.window.labels['profit'][3].set_markup(f'<span foreground="yellow">{pnl:8,.2f}$</span>')
            if not self.window.labels['margin'][3].has_focus():
                self.window.labels['margin'][3].set_text(f'{pos["isolatedMargin"]:6,.2f}')

    def info_orders(self, data):
        for item in self.window.listbox2:
            self.window.listbox2.remove(item)
        data = sorted(data.items(), key=lambda x: x[1]['price'], reverse=True)
        for t in data:
            self.window.listbox2_add(t[1])
        self.window.listbox2.show_all()

    def info_trades(self, data):
        # clear all
        #for item in self.window.listbox1:
        #    self.window.listbox1.remove(item)
        for t in data:
            if t['time']+t['orderId'] in self.window.listbox1_data:
                continue
            self.window.listbox1_add(t)
            self.window.listbox1_data.add(t['time']+t['orderId'])
        self.window.listbox1.show_all()

    def info_stats1(self, data):
        if 'tramos2' in data:
            txt = 'volume by minute ------------\n'
            for d in data['tramos2']:
                txt += f'Last {d["segs"]:2.0f} min    ' + \
                        f'{round(d["diff"]/1000,0):7,.0f}k' + f' <i>({d["percent"]:3.0f}%)</i>\n'
            self.window.stats2.set_markup(txt)
        if 'tramos1' in data:
            txt = 'time---- ----diff --volume\n'
            for d in data['tramos1']:
                txt += f'{d["time"]}  {round(d["diff"]/1000, 0):7,.0f}k {round(d["vol"]/1000, 0):7,.0f}k \n'
            self.window.stats1.set_markup(txt)

    def info_stats2(self, data):
        txt = 'Alert? '+datetime.now().isoformat()[11:]+'\n'
        txt += f'rt buy {data["rt"]["sell"]:9,.0f} sell {data["rt"]["buy"]:9,.0f}\n'
        if 'last1hs' in data["stats"]:
            txt += f'1h diff/s:{data["stats"]["last1hs"]["diff"]:9,.0f}\n'
            txt += f'1h vol/s:{data["stats"]["last1hs"]["vol"]:10,.0f}\n'
            txt += f'p: {min(data["rt"]["price_quant"][1:]):9,.2f} - {max(data["rt"]["price_quant"]):9,.2f}'
        m = max([abs(x) for x in data["rt"]['diff_quant']])
        for d in enumerate(reversed(data["rt"]['diff_quant'])):
            print(f'{d[0]}: {d[1]:10,.0f}  p: {data["rt"]["price_quant"][9-d[0]]}')
            v = min(1,abs(d[1])/m)
            if d[1] < 0:
                self.window.walert[d[0]].modify_bg(Gtk.StateType.NORMAL, Gdk.RGBA(v, v/3, 0, 1).to_color())
            elif d[1] > 0:
                self.window.walert[d[0]].modify_bg(Gtk.StateType.NORMAL, Gdk.RGBA(0, v, v/3, 1).to_color())
            else:
                self.window.walert[d[0]].modify_bg(Gtk.StateType.NORMAL, Gdk.RGBA(0, 0, 0, 1).to_color())
        self.window.stats3.set_markup(txt)


    # TODO: button to close
    def close_app(self, _):
        global GUI_EXIT
        print("quittt???")
        Gtk.main_quit()
        GUI_EXIT = True

    # click events -----------------------------------------

    def b_position_close(self, _):
        self.tasks.put_nowait({'cmd': 'close_position'})
    def b_position_margin(self, _):
        self.tasks.put_nowait({'cmd': 'change_margin', 'amount': float(self.window.labels['margin'][3].get_text())})
    def b_order_close(self, orderid, _):
        print("close_order", orderid)
        self.tasks.put_nowait({'cmd': 'close_order', 'id': orderid})
    def b_orders_close_all(self, _):
        self.tasks.put_nowait({'cmd': 'close_order_all'})
    def b_orders_close_shorts(self, _):
        self.tasks.put_nowait({'cmd': 'close_order_shorts'})
    def b_orders_close_longs(self, _):
        self.tasks.put_nowait({'cmd': 'close_order_longs'})
    def b_get_price_gauge(self, secs, _):
        print('get_price_gauge', secs)
        self.tasks.put_nowait({'cmd': 'price_gauge', 'data': secs})
    def b_set_price_wide(self, units, _):
        self.tasks.put_nowait({'cmd': 'price_wide', 'data': units})
    def b_set_price_adjust(self, units, _):
        self.tasks.put_nowait({'cmd': 'price_adjust', 'data': units})
    def b_amount_increase(self, amnt, _):
        print(f"amount increase > {amnt}")
        self.tasks.put_nowait({'cmd': 'amount_increase', 'data': amnt})
    def b_amount_reset(self, _):
        self.tasks.put_nowait({'cmd': 'amount_reset'})
    def b_focus_order_amount(self, _, w):
        self.window.order_amount.set_text('')
        return True
    def b_focus_order_price1(self, _, w):
        self.window.order_price1.set_text('')
    def b_focus_order_price2(self, _, w):
        self.window.order_price2.set_text('')
    def b_post_long(self, _):
        try:
            price = float(self.window.order_price1.get_text())
        except:
            return
        try:
            qty = float(self.window.order_amount.get_text())
        except:
            return
        self.tasks.put_nowait({'cmd': 'post_order', 'data': (price, qty)})
    def b_post_short(self, _):
        try:
            price = float(self.window.order_price2.get_text())
        except:
            return
        try:
            qty = float(self.window.order_amount.get_text())
        except:
            return
        self.tasks.put_nowait({'cmd': 'post_order', 'data': (price, qty*-1)})
    def b_post_short_market(self, _):
        try:
            qty = float(self.window.order_amount.get_text())
        except:
            return
        self.tasks.put_nowait({'cmd': 'post_order_market', 'data': ('SELL', qty)})

    def b_post_long_market(self, _):
        try:
            qty = float(self.window.order_amount.get_text())
        except:
            return
        self.tasks.put_nowait({'cmd': 'post_order_market', 'data': ('BUY', qty)})

    def figure1_refresh(self, df):
        self.window.figure1['ax'].relim()
        self.window.figure1['ax'].autoscale_view()
        self.window.figure1['figure'].canvas.draw()
        self.window.figure1['figure'].canvas.flush_events()

    def depth1(self, data):
        if 'depth_btcusdt' not in data[0]:
            return
        df = data[1][(data[1].price>self.limits[0]) & (data[1].price<self.limits[1])]
        self.window.figure1['data1'].set_xdata(df.price.values)
        self.window.figure1['data1'].set_ydata(df.q.values)
        self.figure1_refresh(data[1])

    def depth2(self, data):
        df = data[1][(data[1].price>self.limits[0]) & (data[1].price<self.limits[1])]
        self.window.figure1['data2'].set_xdata(df.price.values)
        self.window.figure1['data2'].set_ydata(df.q.values)
        self.figure1_refresh(data[1])

    def depth3(self, data):
        df = data[1][(data[1].price>self.limits[0]) & (data[1].price<self.limits[1])]
        self.window.figure1['data3'].set_xdata(df.price.values)
        self.window.figure1['data3'].set_ydata(df.q.values)
        self.figure1_refresh(data[1])

    def depth4(self, data):
        df = data[1][(data[1].price>self.limits[0]) & (data[1].price<self.limits[1])]
        self.window.figure1['data4'].set_xdata(df.price.values)
        self.window.figure1['data4'].set_ydata(df.q.values)
        self.figure1_refresh(data[1])

    def update(self):
        while True:
            if TASKS.empty():
                break
            t = TASKS.get()
            #print(f"gui > {t['cmd']}")
            self.__getattribute__(t['cmd'])(t['data'])
            time.sleep(0.001)

    def exit(self, _):
        global GUI_EXIT
        Gtk.main_quit()
        GUI_EXIT = True

def init(tasks_main):
    global GUI, TASKS_MAIN
    TASKS_MAIN = tasks_main
    GUI = Gui(TASKS_MAIN)
    time.sleep(.1)
    GUI.start()


if __name__ == "__main__":
    pass