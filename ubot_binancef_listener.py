"""
Testing ubot

Subscribe to Binance futures ws stream
Trades and orderbook
"""

import ubot.comm as comm

import sys
import trio
from trio_websocket import open_websocket_url
from datetime import datetime
import msgpack
from time import time
import json
import asks
import pandas as pd
from queue import Queue
from itertools import chain

MARKETS = ['BTCUSDT', 'EOSUSDT']

class Main():
	def __init__(self):
		self.publisher = comm.Publisher(port=15001)  # feed server
		self.count = 0
		self.exit = False
		self.depth_raw = {}
		self.depth_lastupdateid = {}
		self.depth_last_u = {}
		self.depth_bids = {}
		self.depth_asks = {}
		self.depth_update_feed = {}
		self.watchdog = {'depth': 0}
		for m in MARKETS:
			self.depth_raw[m] = Queue()
			self.depth_lastupdateid[m] = 0
			self.depth_last_u[m] = None
			self.depth_bids[m] = {}
			self.depth_asks[m] = {}
			self.depth_update_feed[m] = True

	async def market_subscribe(self):
		await self.ws_trades.send_message(json.dumps({
			"method": "SUBSCRIBE",
			"params": [m.lower()+"@aggtrade" for m in MARKETS],
			"id": 1
		}))

	async def _watchdog(self):
		while True:
			await trio.sleep(5)
			if time() - self.watchdog['depth'] > 5:
				self.cancel_scope.cancel()
				
	async def _recv_trades(self):
		while True:
			try:
				msg = await self.ws_trades.get_message()
			except Exception as err:
				break
			try:
				pass
			except Exception as err:
				print("* error"+err.__repr__()+msg.__repr__())

	async def _recv_depth(self):
		while True:
			data = None
			try:
				msg = await self.ws_depth.get_message()
				data = json.loads(msg)
			except Exception as err:
				print("_recv_depth  Exception")
				break
			try:
				market = data['stream'][:data['stream'].find('@')].upper()
				if data['data']['u'] < self.depth_lastupdateid[market]:
					continue
				if self.depth_last_u[market] is not None and data['data']['pu'] != self.depth_last_u[market]:
					await self.get_init_depth()
				self.depth_last_u[market] = data['data']['u']
				self.depth_raw[market].put_nowait([data['data']['b'], data['data']['a']])
				self.watchdog['depth'] = time()
			except Exception as err:
				print("* error"+err.__repr__()+msg.__repr__())

	async def get_init_depth(self):
		for m in MARKETS:
			self.depth_asks[m] = {}
			self.depth_bids[m] = {}
			req = await asks.get(f'https://fapi.binance.com/fapi/v1/depth?symbol={m.lower()}&limit=1000')
			data = json.loads(req.text)
			self.depth_lastupdateid[m] = data['lastUpdateId']
			self.depth_raw[m].put_nowait([data['bids'], data['asks']])

	async def _process_depth(self):
		while True:
			for m in MARKETS:
				if self.depth_raw[m].empty():
					await trio.sleep(.1)
					continue
				data = self.depth_raw[m].get()
				for d in data[0]:
					p = float(d[0])
					q = float(d[1])
					if q == 0 and p in self.depth_bids[m]:
						del self.depth_bids[m][p]
					elif q > 0:
						self.depth_bids[m][p] = float(d[1])
				for d in data[1]:
					p = float(d[0])
					q = float(d[1])
					if q == 0 and p in self.depth_asks[m]:
						del self.depth_asks[m][p]
					elif q > 0:
						self.depth_asks[m][p] = float(d[1])
				self.depth_update_feed[m] = True

	async def _feed_depth(self):
		while True:
			await trio.sleep(2)
			for m in MARKETS:
				if not self.depth_update_feed[m]:
					continue
				data = {'type': [], 'price': [], 'q': []}
				bids = {k: self.depth_bids[m][k] for k in sorted(self.depth_bids[m].keys())}
				asks = {k: self.depth_asks[m][k] for k in sorted(self.depth_asks[m].keys())}
				s = 0
				for d in reversed(bids.items()):
					data['type'].append('bid')
					data['price'].append(d[0])
					s += d[1]
					data['q'].append(s)
				s = 0
				for d in asks.items():
					data['type'].append('ask')
					data['price'].append(d[0])
					s += d[1]
					data['q'].append(s)
				p1 = pd.DataFrame(data).sort_values(by='price')
				msg = msgpack.packb(p1.to_dict(), use_bin_type=True)
				await self.publisher.send('depth_'+m.lower(), msg)
				self.depth_update_feed[m] = False  # reset

	async def start(self):
		print(f'starting server {SERVER}')
		self.pd_trades = pd.DataFrame([], columns=['time', 'price', 'buy', 'sell'])
		self.pd_depth = pd.DataFrame([], columns=['time', 'price', 'buy', 'sell'])
		while True:
			await self.get_init_depth()
			async with open_websocket_url("wss://fstream.binance.com/ws") as self.ws_trades:
				await self.market_subscribe()
				async with open_websocket_url("wss://fstream.binance.com/stream?streams=btcusdt@depth") as self.ws_depth:
					async with trio.open_nursery() as nur:
						self.cancel_scope = nur.cancel_scope
						nur.start_soon(self._recv_depth)
						nur.start_soon(self._recv_trades)
						nur.start_soon(self._process_depth)
						nur.start_soon(self._feed_depth)
						nur.start_soon(self._watchdog)
					print("exiting nursery")
			if self.exit:
				print("exiting nursery2")
				break
			print("exiting nursery3")
			await trio.sleep(2)
			# reconnecting
	
if __name__ == '__main__':
	if len(sys.argv) < 2:
		SERVER = False
	else:
		SERVER = sys.argv[1].lower() == 'server'
	trio.run(Main().start)
